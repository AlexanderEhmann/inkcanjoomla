jQuery(function() {


	

	/*
	global variables that might come handy
	*/
	var thisDomain = "";
	var pathToSoundFiles = "audio/";
	var topScrollingSpace = 140;
	var currentPath = window.location+" ";
	var html5_audiotypes={ 
		"ogg": "audio/ogg"	
	}
	
	var ua = window.navigator.userAgent;
	if (!!(ua.match(/msie/i))) {
		checkCookie("UA", "IE");
	}
	if (!!(ua.match(/Trident.*rv\:11\./))) {
		checkCookie("UA", "IE11");
	}
	// "mp4": "audio/mp4",
	// "wav": "audio/wav"
	
	

	if ((currentPath+" ").indexOf('#') !== -1) { 
		/* currentPath + " " so that the not just part of the anchor's name matches later ... */
		jQuery(".scroll-section").each(function() {
			/* iterate all sections */
			if (currentPath.indexOf("#"+jQuery(this).attr('id')+" ") !== -1) {
				/* activate the if it's a match */
				jQuery(this).addClass("active");
			 }
			 else{
				/* reset all that don't match */
				jQuery(this).removeClass("active");
			 }
		});
	 } 
	 
	/* catch click event for scrolling objects */
    jQuery('a.page-scroll').bind('click', function(event) {
		/* scroll to the active link and minus the minimum topScrollingSpace */
		jQuery('html, body').stop().animate({
					scrollTop: jQuery(this).offset().top - topScrollingSpace
				}, 1200, 'easeInOutExpo');

		var sActive = jQuery(this).closest("section.scroll-section").attr('id');
		if (sActive !== undefined) {
			jQuery(".scroll-section").removeClass("active");
			jQuery("#"+sActive).addClass("active");
		}
    });

	/* create a sound for ubaplayer */
	var Snd = {
		init: function() {
		jQuery("audio").each(function() {
		  var src = this.getAttribute('src');
		  if (src.substring(0, 4) !== "snd/") { return; }
		  // Cut out the basename (strip directory and extension)
		  var name = src.substring(4, src.length - 4);
		  // Create the helper function, which clones the audio object and plays it
		  var Constructor = function() {};
		  Constructor.prototype = this;
		  Snd[name] = function() {
			var clone = new Constructor();
			clone.play();
			// Return the cloned element, so the caller can interrupt the sound effect
			return clone;
		  };
		});
		}
	};
	
	
	function createsoundbite(sound){
		var html5audio=document.createElement('audio')
		if (html5audio.canPlayType){ //check support for HTML5 audio
			for (var i=0; i<arguments.length; i++){
				var sourceel=document.createElement('source')
				sourceel.setAttribute('src', arguments[i])
				if (arguments[i].match(/\.(\w+)jQuery/i))
					sourceel.setAttribute('type', html5_audiotypes[RegExp.jQuery1])
				html5audio.appendChild(sourceel)
			}
			html5audio.load()
			html5audio.playclip=function(){
				html5audio.pause()
				//html5audio.currentTime=0
				html5audio.play()
			}
			return html5audio
		}
		else{
			return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
		}
	}
	
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
		}
		return "";
	}

	function checkCookie(cname, cvalue) {
		var cookieContent = getCookie(cname);
		if (cookieContent !== "") {
			setCookie(cname, cvalue, 1);
			
		} 
		else {
			cookieContent = cvalue;
			if (cookieContent !== "" && cookieContent !== null) {
				setCookie(cname, cvalue, 1);
				jQuery(".browser-ie").show();
			}
		}
	}
	
	// ugly todo fixeme to override Joomla's behaviour
	jQuery('.login-status div').removeClass("jmoddiv");
	
	// setup sound for non-IE browsers
	if (!(ua.match(/msie/i) || ua.match(/trident/i) )) {
		
		var sfxBlop=createsoundbite(thisDomain+pathToSoundFiles+"blop.ogg");
		var sfxSlide=createsoundbite(thisDomain+pathToSoundFiles+"turn.ogg");
		
		// an call ubaplayer
		jQuery("#audio-player").ubaPlayer({
			audioButtonClass: 'btn-audio',
			autoPlay: null,
			codecs: [{
				name: 'OGG',
				codec: 'audio/ogg; codecs="vorbis"'
			}, {
				name: 'MP3',
				codec: 'audio/mpeg'
			}],
			continuous: false,
			controlsClass: 'audio-controls',
			extension: null,
			fallbackExtension: '.mp3',
			fallbackFunctions: {
				error: null,
				pause: null,
				play: null,
				resume: null
			},
			flashAudioPlayerPath: 'swf/player.swf',
			flashExtension: '.mp3',
			flashObjectID: 'audio-flash',
			loadingClass: 'audio-loading',
			loop: false,
			playerContainer: 'audio-player',
			playingClass: 'btn-audio-playing',
			swfobjectPath: 'js/swfobject.js',
			volume: 0.5,
		});
		
		// attach handler for sound events 
		jQuery('body button, body a').mouseover(function(event) {
			sfxBlop.playclip();
		});
		jQuery('.tools button, .carousel-control, .carousel-indicators li, .accordion-vertical a.page-scroll, .panel-title a,.accordion-toggle, a.close').click(function(event) {
			sfxSlide.playclip();
		});
		

	}
	jQuery('.browser-ie a.btn').click(function(event) {
			jQuery(".browser-ie").hide();
		});
	
});






