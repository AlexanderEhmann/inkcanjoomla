<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

JHtml::_('bootstrap.framework', false);

$menu = $app->getMenu()->getActive();
$pageclass   = "";


if (is_object( $menu )) :
   $pageclass = $menu->params->get('pageclass_sfx');
 endif; 





if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
//JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/gecko.css');

unset($doc->_css[JURI::root(true) . '/media/jui/css/chosen.css']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<script src="<?php echo ('templates/'.$this->template);?>/js/jquery-1.11.0.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	<jdoc:include type="head" />
	<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/gecko.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body id="page-top" class="site index canvas-outer <?php echo $option
	. ' view-' . $view
	. ($itemid ? ' itemid-' . $itemid : '');?>
	lang-<?php echo $this->language.' page-'.htmlspecialchars($pageclass);?>">


	<div class="canvas">
		
		<div id="navigation">
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container ">
					<div class="navbar-header page-scroll">
						<?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>
						
						
						<a class="navbar-brand " href="<?php echo $this->baseurl; ?>#page-top"><?php echo $sitename; ?></a>
						<div class="btn-group pull-right">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#toolsContainer">
								<span class="sr-only">Toggle login</span>
								<i class="fa fa-login-in "></i>
							</button>
	
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<i class="fa fa-bars"></i>
							</button>
						</div>

					</div>
					<div class="collapse navbar-collapse" id="navbar-collapse-1">
						<div class="tools pull-right">
							<jdoc:include type="modules" name="login-toggle" style="xhtml" />
						</div>
						
						<?php if ($this->countModules('menu')) : ?>
							<jdoc:include type="modules" name="menu" style="none" />
						<?php endif; ?>
					</div>
					<div class=" col-sm-6 col-sm-offset-3 " >
						<div class="accordion-group tools-container " >
							<div class="collapse " id="toolsContainer">
								<div class="panel-group" id="accordionTransversal">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordionTransversal" href="#accordionTransversal1">
												<?php echo JText::_('TPL_GECKO_LOGIN_LOGOUT'); ?>
												</a>
												</h4>
											</div>
											<div id="accordionTransversal1" class="panel-collapse collapse in"><!---->
												<div class="panel-body">
													<jdoc:include type="modules" name="login" style="xhtml" />
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordionTransversal" href="#accordionTransversal2">
												<?php echo JText::_('TPL_GECKO_SEARCH_TITLE'); ?>
												</a>
												</h4>
											</div>
											<div id="accordionTransversal2" class="panel-collapse collapse "><!---->
												<div class="panel-body">
													<jdoc:include type="modules" name="search" style="xhtml" />
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</nav>			
		</div>
		
		<header>
			<div id="header">
				<div class="header">
					
					<div class="container">
						<?php if ($this->countModules('banner')) : ?>
							<jdoc:include type="modules" name="banner" style="none" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			
		</header>
		
		
		<div class="container canvas-content">
			<div class="tag-cloud-vertical">
				<div class="tag-cloud">
					<jdoc:include type="modules" name="cloud" style="none" />
				</div>
			</div>
			<div class="login-status container">
				<div class="wrapper">
					<jdoc:include type="modules" name="login-status" style="xhtml" />
				</div>
			</div>
			<article id="main-article">
				<section>
					<div class="main-content">
					
						<jdoc:include type="message" />
						<jdoc:include type="component" />
					
						<div class="row">
							<?php if ($this->countModules('footer-cloud')) : ?>
								<div class=" col-sm-8 tag-cloud-horizontal ">
									<h3><?php echo JText::_('TPL_GECKO_TAGS_ALL'); ?></h3>
									<div class="tag-cloud">
										<jdoc:include type="modules" name="footer-cloud"  />
									</div>	
								</div>
								<div class="col-sm-4  ">
							<?php else : ?>
								<div class="col-sm-12  ">
							<?php endif; ?>
								<?php if ($this->countModules('footer-popular')) : ?>
									<div class="popular">
										<h3><?php echo JText::_('TPL_GECKO_MOST_POPULAR'); ?></h3>
										<jdoc:include type="modules" name="footer-popular" />
									</div>
								<?php endif; ?>
							</div>
							
						</div>
						<div class="row">
							<div class="col-sm-12">
								<jdoc:include type="modules" name="path" />
							</div>
						</div>
					</div>
				</section>
				
			</article>
		</div>
			<footer>
				<div class="container">
					<div class="row footer">
						
						
						<div class="col-md-4">
							<span class="copyright">
								&copy; <?php echo $sitename; ?> <?php echo date('Y');?>
							</span>
						</div>
						<jdoc:include type="modules" name="footer" />
						
					</div>
				</div>
			</footer>
			
		</div>
		<jdoc:include type="modules" name="debug" style="none" />
		
		<script src="<?php echo ('templates/'.$this->template);?>/js/jquery.easing.min.js"></script>
		<script src="<?php echo ('templates/'.$this->template);?>/js/bootstrap.min.js"></script>
		<script src="<?php echo ('templates/'.$this->template);?>/js/jquery.ubaplayer.min.js"></script>
		<script src="<?php echo ('templates/'.$this->template);?>/js/custom.js"></script>
	</body>
</html>
