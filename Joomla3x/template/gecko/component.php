<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$this->language  = $doc->language;
$this->direction = $doc->direction;
$sitename = $app->getCfg('sitename');

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/gecko.css', 'text/css' , 'screen');
$doc->addStyleSheet('templates/'.$this->template.'/css/print.css', 'text/css' , 'all');
unset($doc->_css[JURI::root(true) . '/media/jui/css/chosen.css']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);

// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<jdoc:include type="head" />
<!--[if lt IE 9]>
	<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
<![endif]-->
<style>
	.main-content .hidden-print {
		
		float:right;
		}
	.main-content .hidden-print a {
			border-bottom:0px #000 solid !important;
			outline:none;
			text-decoration:none !important;
		}
	
</style>
</head>
<body class="index canvas-outer print-version " >
	<div class="canvas">
		<header>
			<div id="header">
				<div class="header">
					<div class="container  ">
						<span class="brand-print"><?php echo $sitename; ?></span>
					</div>
				</div>
			</div>
		</header>
		<div class="container canvas-content">
			<div class="main-content">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
		</div>
		
	</div>
</body>
</html>
