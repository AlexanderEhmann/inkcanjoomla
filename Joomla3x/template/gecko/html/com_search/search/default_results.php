<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="display-as-search-results search-results<?php echo $this->pageclass_sfx; ?>">
<?php foreach ($this->results as $result) : ?>
	<div class="item">
		
		<?php if ($result->href) :?>
			<span class="result-count" ><a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>&nbsp;<?php echo $this->pagination->limitstart + $result->count.'.';?></a></span>
			<span class="result-title">
				<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>><?php echo $this->escape($result->title);?></a>
			</span>
		<?php else:?>
			<span class="result-count" style="float:left;"><?php echo $this->pagination->limitstart + $result->count.'. ';?></span>
			<span class="result-title"><?php echo $this->escape($result->title);?></span>
		<?php endif; ?>
	<span class="result-category">
	
	<?php if ($this->params->get('show_date')) : ?>

		<span class="result-created result-created<?php echo $this->pageclass_sfx; ?>">
			

			
			<span class="date">
					
					<?php $dateCreated = $result->created; 
						$temp = preg_replace("/ /", "</span> <span class='year'>", $dateCreated);
						$dateCreated = preg_replace("/year/", "month", $temp, 1);
						echo "<span class='day'>".$dateCreated."</span> "; ?>
					<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
			</span>
		</span>
	<?php endif; ?>
	</span>
	
	
	<span class="result-text">
		<?php 
		$temp = $result->text;
		if(strlen($result->text)<=120) {
			$temp = $result->text;
		}
		else {
			$temp = substr($result->text,0,120) . '...';
		}
		echo $result->text;
		
		
		
		?>
	</span>
	
	</div>
<?php endforeach; ?>
</div>

<div class="pagination">
	<p class="counter pull-right">
				<?php echo $this->pagination->getPagesCounter(); ?>
			</p>
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
