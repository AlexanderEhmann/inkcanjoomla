<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();
?>

<div class="col-sm-12">
	<h1>
		<?php echo JText::_('TPL_GECKO_SEARCH_SEARCH_TITLE'); ?>
	</h1>
</div>
<div class="col-sm-6 col-sm-offset-3 ">
	<h2 class="subheading ">
		<blockquote >
			<p>
				<?php echo $this->escape($this->origkeyword); ?>
			</p>
			<cite style="padding-right:60px;">
				<?php echo $_SERVER["REMOTE_ADDR"]; ?> -
				<span class="date">
						
						<span class="day">
							<?php echo date('h:m');
							?>
							
						</span>
						
					</span>
				</cite>
		</blockquote>
	</h2>
</div>

<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search');?>" method="post" >
	<div class=" row ">
		<div class="col-sm-12 ">
			<div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
				<?php if (!empty($this->searchword)):?>
					<p class="text-center">
						
						<?php echo JText::plural('TPL_GECKO_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span class="badge badge-largest badge-info " >'. $this->total. '</span>');?>
					</p>
					<p>&nbsp;</p>
				<?php endif;?>
			</div>
		</div>
	</div>
	<div class=" row well">
		
	
		<div class="col-sm-3 col-sm-offset-1 ">
			<fieldset class="phrases">
				<!--legend><?php echo JText::_('COM_SEARCH_FOR');?>
				</legend-->
					<div class="phrases-box">
					<?php echo $this->lists['searchphrase']; ?>
					</div>
			</fieldset>
		</div>
		
		<div class="col-sm-3 ">
			<?php if ($this->params->get('search_areas', 1)) : ?>
				<fieldset class="only">
				<!--legend><?php echo JText::_('COM_SEARCH_SEARCH_ONLY');?></legend-->
				<?php foreach ($this->searchareas['search'] as $val => $txt) :
					$checked = is_array($this->searchareas['active']) && in_array($val, $this->searchareas['active']) ? 'checked="checked"' : '';
				?>
		
				<label for="area-<?php echo $val;?>" class="checkbox">
					<input type="checkbox" name="areas[]" value="<?php echo $val;?>" id="area-<?php echo $val;?>" <?php echo $checked;?> >
					<?php echo JText::_($txt); ?>
				</label>
				<?php endforeach; ?>
				</fieldset>
			<?php endif; ?>
		</div>
		<div class="col-sm-5 ">
			<fieldset class="phrases  ">
					<div class="ordering-box" >
						
						<?php echo $this->lists['ordering'];?>
						<!--label for="ordering" class="ordering">
							
							<?php echo JText::_('TPL_GECKO_SEARCH_DISPLAY_SORT');?>
						</label-->
					</div>
			</fieldset>
			<br/>
			<div class="btn-toolbar ">
				<div class="btn-group ">
					<input type="text" name="searchword" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" id="search-searchword" size="32" maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>" class="inputbox" />
				</div>
				<div class="btn-group pull-left">
					<button name="Search" onclick="this.form.submit()" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('COM_SEARCH_SEARCH');?>"><i class="fa fa-search"></i></button>
				</div>
				<input type="hidden" name="task" value="search" />
				<div class="clearfix"></div>
			</div>
			
		</div>
	</div>
		

	<div class="row">
		<?php if ($this->total > 0) : ?>
		
			<div class="form-limit pull-right">
			
				<?php echo $this->pagination->getLimitBox(); ?>
				<label for="limit">
					&nbsp;<?php echo JText::_('TPL_GECKO_RESULTS_DISPLAY'); ?>
				</label>
			</div>
			
		
		<?php endif; ?>
	</div>
	
</form>
