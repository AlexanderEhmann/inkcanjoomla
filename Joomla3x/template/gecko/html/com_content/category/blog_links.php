<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php
	foreach ($this->link_items as &$item) :
?>
	<li >
		<div class="blog-image">
			
		</div>
		<div class="blog-panel">
			<div class="blog-heading">
				<span class="date">
							
					<span class="day">
						<?php echo (JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC1'))); ?>
					</span>
					<span class="month">
						<?php echo (JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC2'))); ?>
					</span>
					<span class="year">
						<?php echo (JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4'))); ?>
					</span>
					
				</span>
			
			
					
		
				<h4 class="subheading">
					<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid)); ?>"><span class="title title-shortened" itemprop="name"><?php echo $item->title; ?></span></a>
				</h4>
				
			</div>
			
		</div>
		
		
	</li>
<?php endforeach; ?>

