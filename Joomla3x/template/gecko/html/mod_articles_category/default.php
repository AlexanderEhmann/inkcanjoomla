<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="blog-preview">
	<ul class="blog-preview">
		<?php if ($grouped) : ?>
			<?php foreach ($list as $group_name => $group) : ?>
			<li >
				<ul class="category-module<?php echo $moduleclass_sfx; ?> blog-preview ">
		
					<?php foreach ($group as $item) : ?>
						<li>
							<?php if ($params->get('link_titles') == 1) : ?>
								<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
									<?php echo $item->title; ?>
								</a>
							<?php else : ?>
								<?php echo $item->title; ?>
							<?php endif; ?>
							
							<?php if ($item->displayHits) : ?>
								<span class="mod-articles-category-hits">
									(<?php echo $item->displayHits; ?>)
								</span>
							<?php endif; ?>
		
							<?php if ($params->get('show_author')) : ?>
								<span class="mod-articles-category-writtenby">
									<?php echo $item->displayAuthorName; ?>
								</span>
							<?php endif;?>
		
							<?php if ($item->displayCategoryTitle) : ?>
								<span class="mod-articles-category-category">
									(<?php echo $item->displayCategoryTitle; ?>)
								</span>
							<?php endif; ?>
		
							<?php if ($item->displayDate) : ?>
								<span class="mod-articles-category-date"><?php echo $item->displayDate; ?></span>
							<?php endif; ?>
		
							<?php if ($params->get('show_introtext')) : ?>
								<p class="mod-articles-category-introtext">
									<?php echo $item->displayIntrotext; ?>
								</p>
							<?php endif; ?>
		
							<?php if ($params->get('show_readmore')) : ?>
								<p class="mod-articles-category-readmore">
									<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
										<?php if ($item->params->get('access-view') == false) : ?>
											<?php echo JText::_('MOD_ARTICLES_CATEGORY_REGISTER_TO_READ_MORE'); ?>
										<?php elseif ($readmore = $item->alternative_readmore) : ?>
											<?php echo $readmore; ?>
											<?php echo JHtml::_('string.truncate', $item->title, $params->get('readmore_limit')); ?>
												<?php if ($params->get('show_readmore_title', 0) != 0) : ?>
													<?php echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit')); ?>
												<?php endif; ?>
										<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
											<?php echo JText::sprintf('MOD_ARTICLES_CATEGORY_READ_MORE_TITLE'); ?>
										<?php else : ?>
											<?php echo JText::_('MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
											<?php echo JHtml::_('string.truncate', ($item->title), $params->get('readmore_limit')); ?>
										<?php endif; ?>
									</a>
								</p>
							<?php endif; ?>
						</li>
					<?php endforeach; ?>
				</ul>
			</li>
			<?php endforeach; ?>
		<?php else : ?>
			<?php foreach ($list as $item) : ?>
				<li>
					<div class="blog-image" >
					<?php  $images = json_decode($item->images);
					if (isset($images->image_intro) and !empty($images->image_intro)) {
						$imgfloat = (empty($images->float_intro)) ? $params->get('float_intro') : $images->float_intro;
						$class = (htmlspecialchars($imgfloat) != 'none') ? ' class="size-auto align-'.htmlspecialchars($imgfloat).'"' : ' class="size-auto"';
						$title = ($images->image_intro_caption) ? ' title="'.htmlspecialchars($images->image_intro_caption).'"' : '';?>
						
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>"><img class="img-responsive" 
								<?php 
									echo 'src="'.htmlspecialchars($images->image_intro).'" alt="'.htmlspecialchars($images->image_intro_alt).'" /></a>';
								
						} 
						else { ?>
						<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
								<?php 
									echo '</a>';
						
						}
						?>
					</div>
					
					<div class="blog-panel">
						<div class="blog-heading">
							
							<?php $item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
							<?php echo $item->tagLayout->render($item->tags->itemTags); ?>
						
							<?php if ($item->displayDate) : ?>
							<span class="mod-articles-category-date date">
								<span class="day">
									<?php echo (JHtml::_('date', $item->displayDate, JText::_('DATE_FORMAT_LC1')));?>
								</span>
								<span class="month">
									<?php echo (JHtml::_('date', $item->displayDate, JText::_('DATE_FORMAT_LC2')));?>
								</span>
								<span class="year">
									<?php echo (JHtml::_('date', $item->displayDate, JText::_('DATE_FORMAT_LC4')));?>
								</span>
								
							</span>
							<?php endif; ?>
							
							
							
							<h4 class="subheading">
								
								
								<?php if ($params->get('link_titles') == 1) : ?>
									<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>"><span class="title title-shortened"><?php echo $item->title; ?></span></a>
								<?php else : ?>
									<?php echo $item->title; ?>
								<?php endif; ?>
								
							</h4>
						</div>
						
						<div class="blog-body">
							<p class="mod-articles-category-introtext">
								<?php if ($params->get('show_introtext')) : ?>
									<?php echo $item->displayIntrotext; ?>
								<?php endif; ?>
				
								<?php if ($item->displayHits) : ?>
									<span class="mod-articles-category-hits">
										(<?php echo $item->displayHits; ?>)
									</span>
								<?php endif; ?>
					
								<?php if ($params->get('show_author')) : ?>
									<span class="mod-articles-category-writtenby">
										<?php echo $item->displayAuthorName; ?>
									</span>
								<?php endif;?>
					
								<?php if ($item->displayCategoryTitle) : ?>
									<span class="mod-articles-category-category">
										(<?php echo $item->displayCategoryTitle; ?>)
									</span>
								<?php endif; ?>
					
								<?php if ($params->get('show_readmore')) : ?>
									<span class="mod-articles-category-readmore">
										<a class="mod-articles-category-title <?php echo $item->active; ?>" href="<?php echo $item->link; ?>">
											<?php if ($item->params->get('access-view') == false) : ?>
												<?php echo JText::_('TPL_GECKO_MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
											<?php elseif ($readmore = $item->alternative_readmore) : ?>
												<?php echo $readmore; ?>
												
											<?php elseif ($params->get('show_readmore_title', 0) == 0) : ?>
												33333<?php echo JText::sprintf('TPL_GECKO_MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
											<?php else : ?>
												<?php echo JText::_('TPL_GECKO_MOD_ARTICLES_CATEGORY_READ_MORE'); ?>
											<?php endif; ?>
										</a>
									</span>
								<?php endif; ?>
							
							</p>
						</div>
					</div>
				
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>
</div>
