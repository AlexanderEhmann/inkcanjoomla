<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="registration-complete<?php echo $this->pageclass_sfx;?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
	<?php else:?>
		<h1>
			<?php echo JText::_('TPL_GECKO_JREGISTER_REGISTER_USER_TITLE'); ?>
		</h1>
		<h2 class="subheading">
			<?php echo JText::_('TPL_GECKO_JREGISTER_REGISTER_USER_SUCCESS'); ?>
		</h2>
	
	<?php endif; ?>
</div>
