################################################################################README	
#################################################################################

GECKO - responsive template for Joomla 3x
	version 1.0


This document describes how to use the template Gecko with Joomla 3x.

################################################################################LICENSE
This project is published as public domain (GNU GPL). YOu can use, study, share (copy) and modify this software.
	
################################################################################AUDIENCE
Gecko is a template meant to be used with Joomla 3x. It was inspired by the template Protostar. It's intention was to create a template that uses the bootstrap framework for responsive website and makes it available in Joomla without touching Joomla's default components. Install Joomla, the template and the recommended plugins and voila - all done.

You should be familiar with
-	Joomla 3x
	you should know how to use templates, how to install and use plugins.
	Further reference can be found here: http://www.joomla.org/

-	LESS
	If you want to extend this template you should be familiar with compiling CSS via LESS.
	Further reference can be found here: http://lesscss.org/
	
-	BOOTSTRAP
	Bootstrap is the a popular HTML, CSS, and JS framework for developing responsive websites
	Further reference can be found here: http://getbootstrap.com/

################################################################################
This README would normally document whatever steps are necessary to get your application up and running.
Since this project is not an application itself, don't mind looking for a complete setup.


################################################################################HOW TO USE
* This repository contains 3 things:
	1 - a HTML dummy with sample contents, using the same styles as the template
	2 - all necessary less/css/js files for further development
	3 - the template folder (ready to be installed)

	
1. The HTML dummy contains the basic prototype of a sample website. The subfolder contains the javascripts, styles and images that are used. The styles were built with a less-precompiler, converting .less-files into .css-files.


2. The subfolder ./less contains the source-files for further development. Basically, the structure of the folder is similar to the folder structure of BOOTSTRAP.

./less/gecko/				subfolder containing the theme for the GECKO template.
							This is where you would be editing files to to enhance the design.
							The file that configures the theme is the file bootstrap.less in the parent folder.
							
./less/editor/				subfolder containing the styles displayed in the backend when editing content with JCE.
							This is where you would be editing files to to enhance the design. 
							
./less/font-awesome-4.1.0/	less-files of Fontawesome, the icon font
							project website: (http://fortawesome.github.io/Font-Awesome/)

./less/jcemediabox/			a less-file containing the styles for popup layers (JCE MediaBox plugin)

./less/chosen/				a less-file for modifiying the styles of the chosen theme (default form elements of Joomla)
							project website: (https://github.com/alxlit/bootstrap-chosen)

The following list will show you which less-file compiles into which css-file:

./less/bootstrap.less					->		./dummy/html/css/gecko.css
./less/editor/editor.less				->		./dummy/html/css/editor.css
./less/jcemediabox/jcemediabox.less		->		./plugins/system/jcemediabox/css/jcemediabox.css
./less/chosen/bootstrap-chosen.less		->		./media/jui/css/chosen.css

If you have a less compiler installed on your computer you can compile the less-files into a css-file. The files have been tested both with npm (http://nodejs.org/) and Winless (http://winless.org/)


3. The template itself. It is packed in a ZIP archive which you can install as such within Joomla's backend (extensions > template > new)


################################################################################
HOW TO USE WITH JOOMLA

The GECKO template was intended to work well with the following plugins:
	
JCE (content editing)
	v2.4.3
	https://www.joomlacontenteditor.net/

JCE MediaBox for Joomla! (popup layer -  standard theme)
	v1.1.18
	https://www.joomlacontenteditor.net/

Optional plugins that are not needed but I like to recommend:

Akeeba backup (amazing backups)
	v4.0.5 
	https://www.akeebabackup.com

AllVideos (embedding videos)
	v4.6.1
	http://www.joomlaworks.net/extensions/free/allvideos

Modules Anywhere (use modules within any editable content)
	v3.6.9
	http://www.nonumber.nl/extensions/modulesanywhere

After installation of Joomla you should first install the plugins JCE and JCE MediaBox. 

Extract the GECKO Template into a temporary folder within the Joomla installation or copy the ZIP-file into this folder.
Then go to the extension menu and choose templates > new
Browse for the created temporary folder and click 'install'

The template HTML overrides were optimized for german language, so good news if you are using the locale de-DE. However the template contains language files for en-GB as well.

At last, you should add some language overrides. You can do this be going to extension languages > overrides > new

add the following overrides
COM_CONTENT_WRITTEN_BY				Autor %s
COM_TAGS_TITLE_FILTER_LABEL			Titel (oder Teil des Titels)
COM_USERS_LOGIN_REMEMBER_ME			eingeloggt bleiben?
DATE_FORMAT_LC1						d
DATE_FORMAT_LC2						M
DATE_FORMAT_LC3						d. M Y
DATE_FORMAT_LC4						Y
MESSAGE								Hinweis
MOD_SEARCH_LABEL_TEXT				nach
MOD_SEARCH_SEARCHBOX_TEXT			Stichwort


Important note
################################################################################1. At some point of this project I felt very creative and used sound files to have effects on the interactive elements. Unfortunately the Internet Explorer browser family does not really support HTML5 sound/video tags the way I had hoped´and I implemented sound as a non-IE feature in the custom.js file. I cam not really happy about this, but the feature can easily be reased from the script.

2. The files of this template are neither compressed, minified or obfuscated at the moment. The reason is a good readibility for beginners abd there are gazillions of tools to do it, so feel free to do so.

3. JCE MediaBox for Joomla! (popup layer)
The Gecko template works fine with the theme 'standard'.
The default popup html file of JCE MediaBox contains unnecessary HTML-Code. An updated can be found in the folder 'Joomla3x'
After installation of JCE MediaBox, you should merge the subfolder 'plugins' with the folder  'plugins' of you Joomla installation folder:
./Joomla3x/plugins/system/jcemediabox/themes/standard/popup.html

4. The main menu-items should have page-class prefixes to work with some of the design features of the design.


Final note
################################################################################I had a lot of fun with this project, I hope you will have the same.
I am a human and humans make mistakes but I am always eager to learn and happy for your feedback!

Alexander
alexander.john.ehmann@gmail.com
